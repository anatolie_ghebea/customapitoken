#!/bin/bash
echo "Merge and commit to production"
current_branch=$(git branch --show-current )
if [ "$current_branch" == "master" ]
then
    echo "Push to remote master...."
    $(git push origin master )
    echo "Switching to the branch production...."
    $(git checkout production )
    echo "Merge into production...."
    $(git merge master )
    echo "Push to remote production...."
    $(git push origin production )
    echo "back to master"
    $(git checkout master )
    : 
else
    echo "You must be on branch master to run this script!"
    exit 1
fi
exit 0