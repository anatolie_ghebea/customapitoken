<?php 

namespace CustomAPIToken;

class AESService {

    /**
     * @Todo implement hash sign check on the encoming aes
     */

    //Define cipher 
    private const DefaultCipher = 'aes-256-cbc';

    public static function encrypt($value, $encryption_key, $initialization_vector  ) {
        if( ! $encryption_key || !$initialization_vector )
            return null;

        $encryption_key = $encryption_key;
        $iv = hex2bin($initialization_vector);
        
        //Data to encrypt 
        return openssl_encrypt($value, self::DefaultCipher, $encryption_key, 0, $iv); 
    }

    public static function decrypt($encrypted_data, $encryption_key, $initialization_vector){

        if( ! $encryption_key || !$initialization_vector )
            return null;
            
        $encryption_key = $encryption_key;
        $iv = hex2bin($initialization_vector);
        //Decrypt data 
        $str = openssl_decrypt($encrypted_data, self::DefaultCipher, $encryption_key, 0, $iv);
        return $str;
    }

}