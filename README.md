# Custom API Token 

This is a helper module for generating cutom tokens. This is a simplyfued version of a JWT token implementation. It's intended to be easily integrated in any PHP project, regrdless of the framework in use.

# How it works 

@todo 
 


# Installation 

To be able to use this collection under a Laravel project, the following steps must be performed.

- move into `app/Services` of your laravel project
- add this repository as a git submodule `git submodule add -b production https://anatolie_ghebea@bitbucket.org/anatolie_ghebea/customapitoken.git`
- reanme the folder `mv customapitoken CustomAPIToken`
- edit the section `autoload` in the `composer.json` in order for the composer to autoload the files. It should look like this 
 
        "autoload": {
            ...
            "psr-4": {
                "App\\": "app/",
                "CustomAPIToken\\": "app/Services/CustomAPIToken"
            },
            ...
        },
- run `composer dump-autoload` to update the list of the autloaded files.

This should be enough.


# Extending 

If this collection is missing a funcitonality that might be useful in the other project, edit this respository than run  `git pull origin production` in the submodule directory 