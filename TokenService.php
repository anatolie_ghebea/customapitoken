<?php 

/**
 * Custom token algorithm
 * 1) create the user claims -> array of data that will be encrypted
 * 2) convert the array in a json string, 
 * 3) encrypt the json string (using AESEncriptionService)
 * 4) create HMAC hash signature for the encrypted string
 * 5) concat the encrypted string with the signature using '.' (the dot char)
 * 
 * Check token validity
 * 1) separte the token on the '.' (dot char)
 * 2) create the HMAC has signature 
 * 3) compare the new signature with the one after the . 
 * 4) Decde the encrypted string with (AESEncriptionService), this step will be needed only on the ServiceProvider to autologin the user
 * 
 */

namespace CustomAPIToken;

use Exception;
use CustomAPIToken\AESService;

class TokenService {

    private $sharedKey;
    private $tokenLongevity;
    private $algorithm;
    private $encryption_key;
    private $initialization_vector;

    /**
     * 
     * @param String $sharedkey         The scret key shared between servers that will be used to generate and verify tokens
     * @param Int   $tokenLongevity     The default amount of time (in minutes) for the token expiration
     * @param String $algorithm         The aglorithm for the hmac function (supported sha256) 
     */
    public function __construct( $sharedKey, $encryption_key, $initialization_vector, $tokenLongevity, $algorithm )
    {
        if( !$sharedKey ){
            throw new Exception('Shared secret key is necessary for the token generation.');
        }

        $this->sharedKey = $sharedKey;
        $this->tokenLongevity = $tokenLongevity ?? 60;
        $this->algorithm = $algorithm ?? 'sha256';
        $this->encryption_key = $encryption_key ?? '';
        $this->initialization_vector = $initialization_vector ?? '';
    }

    /**
     * Generates the token for the given user detailes
     * @param String        $userEmail
     * @param String|Int    $userId
     * @return String       $token
     * @return null         on error
     */
    public function createUserToken($userEmail, $userId){

        if( empty($userEmail) || empty($userId) ){
            return null;
        }
        
        $claims = [
            'email' => $userEmail,
            'issued_at' => time(),
            'expires_at' => time() + ( $this->tokenLongevity * 60 ),
            'uid' => $userId
        ];

        $payload = json_encode($claims);
        
        $encriptedPayload = AESService::encrypt($payload, $this->encryption_key, $this->initialization_vector); 
        $signature = $this->getSignature($encriptedPayload);

        $token = $encriptedPayload.'.'.$signature;
        
        return $token;
    }

    /**
     * 
     * Check the signature of the token
     * 
     * @param String $token     original token
     * @return Bool $valid      
     */
    public function isValidTokenSignature($token){
        $tokenArray = $this->tokenToArray($token);
        
        $currentSignature = $this->getSignature($tokenArray['payloadEncrypted']);

        return (bool) $currentSignature == $tokenArray['signature'];
    }

    /**
     * 
     * Returns the cailms contained inside the token payload
     * 
     * @param String $token     original token
     * @return Array $claims    the values that are encrypted in the token
     * 
     */
    public function getTokenPayload($token){
        if( !$this->isValidTokenSignature($token) ){
            throw new Exception('Invalid token signature');
        }

        $tokenArray = $this->tokenToArray($token);
        if( $payload = AESService::decrypt($tokenArray['payloadEncrypted'], $this->encryption_key, $this->initialization_vector )){
            if( $claims = json_decode($payload, true) ){
                return $claims;
            } 
        }

        throw new Exception('Payload decryption faild.');
    }

    /**
     * Checks if the token is still valid
     * @param Array $cailms
     * @return Bool $expired
     */
    public function isTokenExpired( $claims ){
        // if expires_at is before current time
        return (bool) ( intval($claims['expires_at']) <= intval(time()) );
    }

    /**
     * Returns the hmac signature for the encrypted string
     * @param String $encriptedPayload
     * @return String $signature
     * 
     */
    public function getSignature($encriptedPayload){
        
        $algo = $this->getHashAlgo();
        return hash_hmac($algo, $encriptedPayload, $this->sharedKey);
    }

    /**
     * Splits the token to its subparts
     * @param String $token     
     * @return Array $parts 
     */
    protected function tokenToArray($token){
        $bits = explode('.', $token);

        if( count($bits) != 2 )
            throw new Exception('Invalid token provided');
        
        return [ 'payloadEncrypted' => $bits[0], 'signature' => $bits[1] ];
    }

    /**
     * 
     */
    protected function getHashAlgo(){

        if( $this->algorithm != 'sha256' ){
            throw new Exception('Invalid hash algorihtm for hash_hmac function.');
        }
        return $this->algorithm;
    } 

    public static function base64url_encode($data, $pad = null) {
        $data = str_replace(array('+', '/'), array('-', '_'), base64_encode($data));
        if (!$pad) {
            $data = rtrim($data, '=');
        }
        return $data;
    }

    public static function base64url_decode($data) {
        return base64_decode(str_replace(array('-', '_'), array('+', '/'), $data));
    }
}